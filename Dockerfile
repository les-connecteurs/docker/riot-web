FROM docker.io/vectorim/riot-web:v1.6.3

EXPOSE 80

COPY nginx.conf /etc/nginx/conf.d/default.conf
